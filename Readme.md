#about
sample crud operation for Person

--------STEPS----------
docker-compose build
docker-compose up

For front-end go to:
localhost:3000

For back-end go to:
localhost:8080/swagger

To build and run

        docker-compose up --build

To stop

        docker-compose down