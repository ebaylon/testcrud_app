import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import Swal from 'sweetalert2'
import { Validators, FormControl } from '@angular/forms';
import { PersonService } from '../services/person.service';
import { Person } from '../models/person';

@Component({
    selector: 'dialog-person',
    templateUrl: 'dialog-person.component.html',
    styleUrls: ['dialog-person.component.scss']
})
export class DialogPersonComponent {

    constructor(public dialogRef: MatDialogRef<DialogPersonComponent>,
        @Inject(MAT_DIALOG_DATA) public data: Person,
        public service: PersonService) { }

        gender: Gender[] = [
            {value: 'M', viewValue: 'MALE'},
            {value: 'F', viewValue: 'FEMALE'}           
          ];

    formControl = new FormControl('', [
        Validators.required        
    ]);

    getErrorMessage() {
        return this.formControl.hasError('required') ? 'Required field' :
            this.formControl.hasError('first_name') ? 'Required First Name' :
                this.formControl.hasError('last_name') ? 'Required Last Name' :
                    this.formControl.hasError('gender') ? 'Required Gender' :
                        '';
    }

    save() {
        Swal.fire({
            title: 'Are you sure?',
            text: 'You to save this data?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, save it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                if (this.data.id == null || this.data.id === 0) {
                    this.service.add(this.data).subscribe(
                        () => {
                            Swal.fire(
                                'Saved!',
                                'Your data has been created.',
                                'success'
                            ).then((result2) => {
                                if (result2.value) {
                                    this.dialogRef.close(1);
                                }
                            });
                        },
                        (error: any) => {
                            Swal.fire('Failed!!', error, 'error');
                        });
                } else {
                    this.service.update(this.data.id,this.data).subscribe(
                        () => {
                            Swal.fire(
                                'Saved!',
                                'Your data has been updated.',
                                'success'
                            ).then((result2) => {
                                if (result2.value) {
                                    this.dialogRef.close(1);
                                }
                            });
                        },
                        (error: any) => {
                            Swal.fire('Failed!!', error, 'error');
                        });
                }
            }
        });
    }

    close() {
        this.dialogRef.close();
    }
}

interface Gender {
    value: string;
    viewValue: string;
  }
