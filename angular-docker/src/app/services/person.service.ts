import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Person } from '../models/person';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})
export class PersonService {
    private url = `${environment.API_URL}/Person`; // URL to web api
    dataChange: BehaviorSubject<Person[]> = new BehaviorSubject<Person[]>([]);
    dataList: Person[] = [];
    dialogData: any;  

    get data(): Person[] {
        return this.dataChange.value;
    }

    constructor(private http: HttpClient) { }
   
    // Get List
    getList(): Observable<Person[]> {        
        return this.http.get<Person[]>(this.url);
        
    }

    /** GET by id to the server */
    findOne(id: number): Observable<Person> {
        const url = `${this.url}/${id}`;
        return this.http.get<Person>(url);
    }

    /** POST: add to the server */
    add(model: Person) {
        return this.http.post(`${this.url}`, JSON.stringify(model), httpOptions);
    }

    /** PUT: update to the server */
    update(id: number, model: Person) {
        return this.http.put(`${this.url}/${id}`, JSON.stringify(model), httpOptions);
    }

    /** DELETE: delete to the server */
    delete(id: number) {       
        const url = `${this.url}/${id}`;        
        return this.http.delete(url);
    }

    _errorHandler(error: Response) {
        return;    }
}
