export class Person {
    id: number | undefined;
    first_name: string | undefined;
    last_name: string | undefined;
    gender: string | undefined;
}