import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Observable, merge, of} from 'rxjs';
import { Person } from './models/person';
import { DataSource } from "@angular/cdk/collections";
import Swal from 'sweetalert2'
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PersonService } from './services/person.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogPersonComponent } from './dialog-person/dialog-person.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    this.GetPersonList();
  }
  displayedColumns = ["id", "first_name", "last_name", "gender", "actions"];
  dataSource!: ListDataSource;  
  persons!: Person[];
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;  

  constructor(private dialog: MatDialog,private service: PersonService) { }

  GetPersonList(){
    this.service.getList().subscribe(
      resp => {        
        this.dataSource = new ListDataSource(resp);
        this.persons = resp;
      },
      err => console.log(err),
      () => console.log('Error!')
    );
  }

  onAdd(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true; dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'remove-space';
    dialogConfig.data = {};

    const dialogRef = this.dialog.open(DialogPersonComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result: number) => {
      if (result === 1) { 
        this.GetPersonList();
      }
    });
  }

  onEdit(data: any) {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true; dialogConfig.autoFocus = true;
    dialogConfig.panelClass = 'remove-space';
    dialogConfig.data = data;

    const dialogRef = this.dialog.open(DialogPersonComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result: number) => {
      if (result === 1) {
        this.GetPersonList();
      }
    });
  }

  onDelete(id: number){
    Swal.fire({
      title: 'Are you sure?',
      text: 'You want to Removed this data?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, Removed it!',
      cancelButtonText: 'No, keep it'
    }).then((result) => {
      if (result.value) {       
        this.service.delete(id).subscribe(
          res => {            
            Swal.fire(
              'Removed!',
              'Successfully Deleted!.',
              'success'
            ).then((result2) => {
              if (result2.value) {
                this.GetPersonList();
              }
            });
          },
          error => {           
            Swal.fire('Failed!!',error,'error');
          });
      }
    });
  }
}

class ListDataSource extends DataSource<any> {
  constructor(private list: Person[]) {
    super();
  }

  connect(): Observable<Person[]> {
    return of(this.list);
  }
  disconnect() { }
}
