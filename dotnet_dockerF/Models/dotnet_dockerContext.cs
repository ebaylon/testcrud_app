﻿using Microsoft.EntityFrameworkCore;

namespace dotnet_docker.Models
{
    public class dotnet_dockerContext : DbContext
    {
        public dotnet_dockerContext(DbContextOptions<dotnet_dockerContext> options)
            : base(options)
        {
        }

        public DbSet<Person> Person { get; set; }
    }
}
