﻿namespace dotnet_docker.Models
{
    public class Person
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public char gender { get; set; } = 'M';
    }
}
